/********** AUDIT TABLES **********/
SET search_path to 'audit';

/* DOMAIN type */
CREATE DOMAIN audit.type AS TEXT
CHECK(
   VALUE ~ 'Insert'
OR VALUE ~ 'Update'
OR VALUE ~ 'Delete'
);

/* AUDIT TABLE CUSTOMER_GROUP */
CREATE TABLE audit.customer_group_audit ( 
    id bigserial PRIMARY KEY,
    title varchar(40) NOT NULL,
    is_deleted boolean NOT NULL,
    created_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    modified_date timestamp with time zone,
    created_by bigint,
    modified_by bigint,
    session_id bigint NOT NULL,
    
    audit_id bigserial,
    type audit.type NOT NULL,
    audit_date timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP
);

/* AUDIT TABLE CUSTOMER */
CREATE TABLE audit.customer_audit ( 
    id bigserial PRIMARY KEY,
    first_name varchar(40) NOT NULL,
    last_name varchar(40) NOT NULL,
    login varchar(40) NOT NULL,
    password varchar(100) NOT NULL,
    customer_group_id bigint NOT NULL,
    is_active boolean NOT NULL,
    is_deleted boolean NOT NULL,
    created_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    modified_date timestamp with time zone,  
    created_by bigint,
    modified_by bigint,
    session_id bigint NOT NULL,
    email varchar(40),
    
	audit_id bigserial,
	type audit.type NOT NULL,
	audit_date timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP
);

/* AUDIT TABLE COURSE_TYPE */
CREATE TABLE audit.course_type_audit ( 
    id bigserial PRIMARY KEY,
    title varchar(40) NOT NULL,
    is_deleted boolean NOT NULL,
    created_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    modified_date timestamp with time zone,
    created_by bigint,
    modified_by bigint,
    session_id bigint NOT NULL,
    
    audit_id bigserial,
    type audit.type NOT NULL,
    audit_date timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP
);

/* AUDIT TABLE COURSE */
CREATE TABLE audit.course_audit ( 
    id bigserial PRIMARY KEY,
    title varchar(80) NOT NULL,
    price decimal(5,2) NOT NULL,
    description varchar(200),
    course_type_id bigint NOT NULL,
    created_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    modified_date timestamp with time zone,
    created_by bigint,
    modified_by bigint,
    session_id bigint NOT NULL,
    
    audit_id bigserial,
    type audit.type NOT NULL,
    audit_date timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP
);

/* AUDIT TABLE COURSE_AVAILABILITY */
CREATE  TABLE audit.course_availability_audit ( 
    id bigserial PRIMARY KEY,
    course_id bigint NOT NULL,
    flag boolean NOT NULL,
    course_availability_date date NOT NULL,
    is_active boolean NOT NULL,
    is_deleted boolean NOT NULL,
    created_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    modified_date timestamp with time zone,
    created_by bigint,
    modified_by bigint,
    session_id bigint NOT NULL,
    
    audit_id bigserial,
    type audit.type NOT NULL,
    audit_date timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP
);

/* AUDIT TABLE COURSES_ORDER */
CREATE  TABLE audit.courses_order_audit ( 
    id bigserial PRIMARY KEY,
    customer_id bigint NOT NULL,
    order_date date NOT NULL,
    delivery_id bigint NOT NULL,
    created_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    modified_date timestamp with time zone,
    created_by bigint,
    modified_by bigint,
    session_id bigint NOT NULL,
    
    audit_id bigserial,
    type audit.type NOT NULL,
    audit_date timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP
);

/* AUDIT TABLE ORDER_DETAILS */
CREATE  TABLE audit.order_details_audit ( 
    id bigserial PRIMARY KEY,
    order_id bigint NOT NULL,
    course_id bigint NOT NULL,
    course_price decimal(5,2) NOT NULL,
    portions int NOT NULL,
    created_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
    modified_date timestamp with time zone,
    created_by bigint,
    modified_by bigint,
    session_id bigint NOT NULL,
    
    audit_id bigserial,
    type audit.type NOT NULL,
    audit_date timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP
);

/* AUDIT TABLE DELIVERY */
CREATE TABLE audit.delivery_audit (
    id bigserial PRIMARY KEY,
    price decimal(5,3) NOT NULL,
    delivery_date date NOT NULL,
    is_deleted boolean NOT NULL,
    created_date timestamp with time zone DEFAULT now(),
    modified_date timestamp with time zone,
    created_by bigint,
    modified_by bigint,
    session_id bigint NOT NULL,

    audit_id bigserial,
    type audit.type NOT NULL,
    audit_date timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP
);
