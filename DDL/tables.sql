/********** TABLES **********/
SET search_path to 'food_order';
SET timezone = 'Europe/Kiev';

ALTER DATABASE postgres SET timezone TO 'Europe/Kiev';

CREATE SCHEMA food_order;
CREATE SCHEMA audit;

/* TABLE CUSTOMER_GROUP */
CREATE TABLE food_order.customer_group (
    id bigserial PRIMARY KEY,
    title varchar(40) NOT NULL,
    is_deleted boolean NOT NULL,
    created_date timestamp with time zone DEFAULT now(),
    modified_date timestamp with time zone,
    created_by bigint,
    modified_by bigint,
    session_id bigint NOT NULL REFERENCES session (id)
);
ALTER TABLE food_order.customer_group ADD CONSTRAINT customer_group_session_id_fkey 
FOREIGN KEY ( session_id ) REFERENCES food_order.session(id);

/* TABLE CUSTOMER */
CREATE TABLE food_order.customer (
    id bigserial PRIMARY KEY,
    first_name varchar(40) NOT NULL,
    last_name varchar(40) NOT NULL,
    login varchar(40) NOT NULL,
    password varchar(100) NOT NULL,
    customer_group_id bigint NOT NULL REFERENCES customer_group (id),
    is_active boolean NOT NULL,
    is_deleted boolean NOT NULL,
    created_date timestamp with time zone DEFAULT now(),
    modified_date timestamp with time zone,
    created_by bigint,
    modified_by bigint,
    session_id bigint NOT NULL REFERENCES session (id),
    email varchar(40) unique
);
ALTER TABLE food_order.customer ADD CONSTRAINT customer_session_id_fkey 
FOREIGN KEY ( session_id ) REFERENCES food_order.session(id);
ALTER TABLE food_order.customer ADD CONSTRAINT customer_customer_group_id_fkey 
FOREIGN KEY ( customer_group_id ) REFERENCES food_order.customer_group(id);

/* TABLE COURSE_TYPE */
CREATE TABLE food_order.course_type (
    id bigserial PRIMARY KEY,
    title varchar(40) NOT NULL,
    is_deleted boolean NOT NULL,
    created_date timestamp with time zone DEFAULT now(),
    modified_date timestamp with time zone,
    created_by bigint,
    modified_by bigint,
    session_id bigint NOT NULL REFERENCES session (id)
);
ALTER TABLE food_order.course_type ADD CONSTRAINT course_type_session_id_fkey 
FOREIGN KEY ( session_id ) REFERENCES food_order.session(id);

/* TABLE COURSE */
CREATE TABLE food_order.course (
    id bigserial PRIMARY KEY,
    title varchar(80) NOT NULL,
    price decimal(5,2) NOT NULL,
    description varchar(200),
    course_type_id bigint NOT NULL REFERENCES course_type (id),
    created_date timestamp with time zone DEFAULT now(),
    modified_date timestamp with time zone,
    created_by bigint,
    modified_by bigint,
    session_id bigint NOT NULL REFERENCES session (id)
);
ALTER TABLE food_order.course ADD CONSTRAINT course_course_type_id_fkey 
FOREIGN KEY ( course_type_id ) REFERENCES food_order.course_type(id);
ALTER TABLE food_order.course ADD CONSTRAINT course_session_id_fkey 
FOREIGN KEY ( session_id ) REFERENCES food_order.session(id);

/* TABLE COURSE_AVAILABILITY */
CREATE TABLE food_order.course_availability (
    id bigserial PRIMARY KEY,
    course_id bigint NOT NULL REFERENCES course (id), 
    flag boolean NOT NULL,
    course_availability_date date NOT NULL,
    week_number bigint NOT NULL,
    is_active boolean NOT NULL,
    is_deleted boolean NOT NULL,
    created_date timestamp with time zone DEFAULT now(),
    modified_date timestamp with time zone,
    created_by bigint, 
    modified_by bigint, 
    session_id bigint NOT NULL REFERENCES session (id) 
);
ALTER TABLE food_order.course_availability ADD CONSTRAINT course_availability_course_id_fkey 
FOREIGN KEY ( course_id ) REFERENCES food_order.course(id);
ALTER TABLE food_order.course_availability ADD CONSTRAINT course_availability_session_id_fkey 
FOREIGN KEY ( session_id ) REFERENCES food_order.session(id);

/* TABLE COURSES_ORDER */
CREATE TABLE food_order.courses_order (
    id bigserial PRIMARY KEY,
    customer_id bigint NOT NULL REFERENCES customer (id),
    order_date date NOT NULL,
    delivery_id bigint NOT NULL REFERENCES delivery(id),
    created_date timestamp with time zone DEFAULT now(),
    modified_date timestamp with time zone,
    created_by bigint,
    modified_by bigint,  
    session_id bigint REFERENCES session (id)
);
ALTER TABLE food_order.courses_order ADD CONSTRAINT courses_order_customer_id_fkey 
FOREIGN KEY ( customer_id ) REFERENCES food_order.customer(id);
ALTER TABLE food_order.courses_order ADD CONSTRAINT courses_order_delivery_id_fkey 
FOREIGN KEY ( delivery_id ) REFERENCES food_order.delivery(id);
ALTER TABLE food_order.courses_order ADD CONSTRAINT courses_order_session_id_fkey 
FOREIGN KEY ( session_id ) REFERENCES food_order.session(id);

/* TABLE ORDER_DETAILS */
CREATE TABLE food_order.order_details (
    id bigserial PRIMARY KEY,
    order_id bigint NOT NULL REFERENCES courses_order (id), 
    course_id bigint NOT NULL REFERENCES course (id),
    course_price decimal(5,2) NOT NULL,
    portions integer NOT NULL,
    created_date timestamp with time zone DEFAULT now(),
    modified_date timestamp with time zone,
    created_by bigint,
    modified_by bigint,
    session_id bigint NOT NULL REFERENCES session (id)
);
ALTER TABLE food_order.order_details ADD CONSTRAINT order_details_order_id_fkey 
FOREIGN KEY ( order_id ) REFERENCES food_order.courses_order(id);
ALTER TABLE food_order.order_details ADD CONSTRAINT order_details_course_id_fkey 
FOREIGN KEY ( course_id ) REFERENCES food_order.course(id);
ALTER TABLE food_order.order_details ADD CONSTRAINT order_details_session_id_fkey 
FOREIGN KEY ( session_id ) REFERENCES food_order.session(id);

/* TABLE SESSION */
CREATE TABLE food_order.session (
    id bigserial PRIMARY KEY,
    customer_id bigint NOT NULL REFERENCES customer (id),
    login_date timestamp with time zone DEFAULT now(),
    logout_date timestamp with time zone
);
ALTER TABLE food_order.session ADD CONSTRAINT session_customer_id_fkey 
FOREIGN KEY ( customer_id ) REFERENCES food_order.customer(id);

/* TABLE DELIVERY */
CREATE TABLE food_order.delivery (
    id bigserial PRIMARY KEY,
    price decimal(5,3) NOT NULL,
    delivery_date date NOT NULL,
    is_deleted boolean NOT NULL,
    created_date timestamp with time zone DEFAULT now(),
    modified_date timestamp with time zone,
    created_by bigint,
    modified_by bigint,
    session_id bigint NOT NULL REFERENCES session (id),
    total_sum decimal(5,3) 
);
ALTER TABLE food_order.delivery ADD CONSTRAINT delivery_session_id_fkey 
FOREIGN KEY ( session_id ) REFERENCES food_order.session(id);
    
CREATE INDEX idx_order_date ON food_order.courses_order(order_date);
CREATE INDEX idx_course_availability_date ON food_order.course_availability(course_availability_date);

CREATE SEQUENCE food_order."temp_id"
INCREMENT BY 1 
START WITH 1 
