/********* UPDATE FUNCTIONS **********/

/* FUNCTION ADMIN UPDATE COURSE TYPE */
CREATE OR REPLACE FUNCTION food_order.fn_admin_update_course_type(
  p_id bigint,
  p_title varchar(40),
  p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;

BEGIN
SET search_path to 'food_order';

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_modified_by;  

IF  EXISTS(SELECT  id 
      FROM   food_order.course_type
      WHERE  p_id = id) THEN
UPDATE food_order.course_type SET title = p_title,
modified_by = d_modified_by
WHERE id = p_id;

ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN UPDATE COURSE AND AVAILABILITY */
CREATE OR REPLACE FUNCTION food_order.fn_admin_update_course_and_availability(
  p_course_id bigint,
  p_title varchar(80),
  p_price decimal(5,2),
  p_course_type_title varchar(40),
  p_description varchar(200),
  p_week_flags int,
  p_start_date date,
  p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;
  d_course_type_id bigint;

  d_monday int;
  d_tuesday int;
  d_wednesday int;
  d_thursday int;
  d_friday int;
  d_saturday int;
  d_sunday int;

  flag_monday boolean; 
  flag_tuesday boolean; 
  flag_wednesday boolean; 
  flag_thursday boolean; 
  flag_friday boolean; 
  flag_saturday boolean; 
  flag_sunday boolean; 

  is_updated boolean DEFAULT 'false';

BEGIN
SET search_path to 'food_order';

SELECT 1 << 1 INTO d_monday;
SELECT 1 << 2 INTO d_tuesday;
SELECT 1 << 3 INTO d_wednesday;
SELECT 1 << 4 INTO d_thursday;
SELECT 1 << 5 INTO d_friday;
SELECT 1 << 6 INTO d_saturday;
SELECT 1 << 7 INTO d_sunday;

IF ((p_week_flags & d_monday) = 0) THEN flag_monday = false; ELSE flag_monday = true; END IF;
IF ((p_week_flags & d_tuesday) = 0) THEN flag_tuesday = false; ELSE flag_tuesday = true; END IF;
IF ((p_week_flags & d_wednesday) = 0) THEN flag_wednesday = false; ELSE flag_wednesday = true; END IF;
IF ((p_week_flags & d_thursday) = 0) THEN flag_thursday = false; ELSE flag_thursday = true; END IF;
IF ((p_week_flags & d_friday) = 0) THEN flag_friday = false; ELSE flag_friday = true; END IF;
IF ((p_week_flags & d_saturday) = 0) THEN flag_saturday = false; ELSE flag_saturday = true; END IF;
IF ((p_week_flags & d_sunday) = 0) THEN flag_sunday = false; ELSE flag_sunday = true; END IF;

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_modified_by;   

SELECT id FROM food_order.course_type
WHERE title = p_course_type_title INTO d_course_type_id;  

IF d_course_type_id IS NOT NULL THEN

IF  EXISTS(SELECT  id 
    FROM   food_order.course
    WHERE  p_course_id = id) THEN

UPDATE food_order.course SET title = p_title,
price = p_price,
course_type_id = d_course_type_id,
description = p_description,
modified_by = d_modified_by
WHERE id = p_course_id;


IF EXISTS(SELECT c_o.id FROM food_order.courses_order AS c_o
WHERE c_o.order_date = p_start_date) THEN
IF ((SELECT flag 
  FROM food_order.course_availability AS c_a 
  WHERE course_id = p_course_id AND course_availability_date = p_start_date) != flag_monday) 
THEN is_updated = 'true'; 
END IF;
END IF;

IF EXISTS(SELECT c_o.id FROM food_order.courses_order AS c_o
WHERE c_o.order_date = p_start_date+1) THEN
IF ((SELECT flag 
  FROM food_order.course_availability AS c_a 
  WHERE course_id = p_course_id AND course_availability_date = p_start_date+1) != flag_tuesday) 
THEN is_updated = 'true'; 
END IF;
END IF;

IF EXISTS(SELECT c_o.id FROM food_order.courses_order AS c_o
WHERE c_o.order_date = p_start_date+2) THEN
IF ((SELECT flag 
  FROM food_order.course_availability AS c_a 
  WHERE course_id = p_course_id AND course_availability_date = p_start_date+2) != flag_wednesday) 
THEN is_updated = 'true'; 
END IF;
END IF;

IF EXISTS(SELECT c_o.id FROM food_order.courses_order AS c_o
WHERE c_o.order_date = p_start_date+3) THEN
IF ((SELECT flag 
  FROM food_order.course_availability AS c_a 
  WHERE course_id = p_course_id AND course_availability_date = p_start_date+3) != flag_thursday) 
THEN is_updated = 'true'; 
END IF;
END IF;

IF EXISTS(SELECT c_o.id FROM food_order.courses_order AS c_o
WHERE c_o.order_date = p_start_date+4) THEN
IF ((SELECT flag 
  FROM food_order.course_availability AS c_a 
  WHERE course_id = p_course_id AND course_availability_date = p_start_date+4) != flag_friday) 
THEN is_updated = 'true'; 
END IF;
END IF;

IF EXISTS(SELECT c_o.id FROM food_order.courses_order AS c_o
WHERE c_o.order_date = p_start_date+5) THEN
IF ((SELECT flag 
  FROM food_order.course_availability AS c_a 
  WHERE course_id = p_course_id AND course_availability_date = p_start_date+5) != flag_saturday) 
THEN is_updated = 'true'; 
END IF;
END IF;

IF EXISTS(SELECT c_o.id FROM food_order.courses_order AS c_o
WHERE c_o.order_date = p_start_date+6) THEN
IF ((SELECT flag 
  FROM food_order.course_availability AS c_a 
  WHERE course_id = p_course_id AND course_availability_date = p_start_date+6) != flag_sunday) 
THEN is_updated = 'true'; 
END IF;
END IF;

UPDATE food_order.course_availability SET flag = flag_monday,
modified_by = d_modified_by,
week_flags = p_week_flags
WHERE course_id = p_course_id AND course_availability_date = p_start_date;
UPDATE food_order.course_availability SET flag = flag_tuesday,
modified_by = d_modified_by,
week_flags = p_week_flags
WHERE course_id = p_course_id AND course_availability_date = p_start_date+1;
UPDATE food_order.course_availability SET flag = flag_wednesday,
modified_by = d_modified_by,
week_flags = p_week_flags
WHERE course_id = p_course_id AND course_availability_date = p_start_date+2;
UPDATE food_order.course_availability SET flag = flag_thursday,
modified_by = d_modified_by,
week_flags = p_week_flags
WHERE course_id = p_course_id AND course_availability_date = p_start_date+3;
UPDATE food_order.course_availability SET flag = flag_friday,
modified_by = d_modified_by,
week_flags = p_week_flags
WHERE course_id = p_course_id AND course_availability_date = p_start_date+4;
UPDATE food_order.course_availability SET flag = flag_saturday,
modified_by = d_modified_by,
week_flags = p_week_flags
WHERE course_id = p_course_id AND course_availability_date = p_start_date+5;
UPDATE food_order.course_availability SET flag = flag_sunday,
modified_by = d_modified_by,
week_flags = p_week_flags
WHERE course_id = p_course_id AND course_availability_date = p_start_date+6;

DELETE FROM food_order.order_details AS o_d
WHERE o_d.order_id IN (SELECT id FROM food_order.courses_order AS c_o WHERE c_o.id = o_d.order_id) 
AND o_d.course_id IN (SELECT course_id FROM food_order.course_availability AS c_a 
WHERE c_a.course_id = o_d.course_id AND c_a.flag = 'false')
AND o_d.course_id = p_course_id AND is_updated = 'true';

ELSE RETURN -1;
END IF;
ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN UPDATE CUSTOMER GROUP */
CREATE OR REPLACE FUNCTION food_order.fn_admin_update_customer_group(
  p_id bigint,
  p_title varchar(40),
  p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;

BEGIN
SET search_path to 'food_order';

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_modified_by;  

IF  EXISTS(SELECT  id 
      FROM   food_order.customer_group
      WHERE  p_id = id) THEN
UPDATE food_order.customer_group SET title = p_title,
modified_by = d_modified_by 
WHERE id = p_id;

ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN UPDATE CUSTOMER */
CREATE OR REPLACE FUNCTION food_order.fn_admin_update_customer(
  p_id bigint,
  p_first_name varchar(40),
  p_last_name varchar(40),
  p_login varchar(40),
  p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;

BEGIN
SET search_path to 'food_order';

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_modified_by;  

IF  EXISTS(SELECT  id 
      FROM   food_order.customer
      WHERE  p_id = id) THEN

UPDATE food_order.customer SET first_name = p_first_name,
last_name = p_last_name,
login = p_login,
modified_by = d_modified_by
WHERE id = p_id;

ELSE RETURN -1;
END IF; 
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ALL UPDATE PASSWORD */
CREATE OR REPLACE FUNCTION food_order.fn_all_update_password(
  p_customer_id bigint,
  p_new_password varchar(100),
  p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;

BEGIN
SET search_path to 'food_order';

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_modified_by; 

IF  EXISTS(SELECT id
      FROM   food_order.customer
      WHERE  p_customer_id = id) THEN
UPDATE food_order.customer SET password = p_new_password,
modified_by = d_modified_by 
WHERE id = p_customer_id;

ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION MOVE COURSES ON NEXT WEEK */
CREATE OR REPLACE FUNCTION food_order.fn_admin_move_course_on_week(
  p_start_date date, 
  p_course_id bigint, 
  p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_created_by bigint;
  d_week_number bigint;
  d_year_number bigint;
  d_week_year_number bigint;
  d_course_id bigint;
  d_course_title varchar(80);
  d_course_price decimal(5,2);
  d_course_description varchar(200);
  d_course_type_id bigint;

BEGIN
SET search_path to 'food_order';

d_week_number = (SELECT extract(week FROM p_start_date::date));
d_year_number = (SELECT extract(year FROM p_start_date::date));
d_week_year_number = (SELECT CAST(CAST(d_week_number AS text)||CAST(d_year_number AS text) AS numeric(24,0)));

SELECT title FROM food_order.course 
WHERE id = p_course_id INTO d_course_title;

IF NOT EXISTS (SELECT c.id FROM food_order.course AS c
  INNER JOIN course_availability AS c_a ON c_a.course_id = c.id
  WHERE title = d_course_title AND c_a.course_availability_date =p_start_date
 AND c_a.is_deleted = 'false'
) THEN

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_created_by;   

SELECT price FROM food_order.course 
WHERE id = p_course_id INTO d_course_price;

SELECT description FROM food_order.course 
WHERE id = p_course_id INTO d_course_description;

SELECT course_type_id FROM food_order.course 
WHERE id = p_course_id INTO d_course_type_id;

INSERT INTO food_order.course(title, price, description, course_type_id,
created_by,  session_id) 
VALUES(d_course_title, d_course_price, d_course_description, d_course_type_id, d_created_by, p_session_id);

d_course_id = (SELECT currval('food_order.course_id_seq'));

INSERT INTO food_order.course_availability(course_id, flag, course_availability_date, week_number, is_active,
    is_deleted, created_by, session_id) VALUES(d_course_id, 'false', p_start_date, d_week_year_number, 'true', 
    'false', d_created_by, p_session_id);
INSERT INTO food_order.course_availability(course_id, flag, course_availability_date, week_number, is_active,
    is_deleted, created_by, session_id) VALUES(d_course_id, 'false', p_start_date+1, d_week_year_number, 'true', 
    'false', d_created_by, p_session_id);
INSERT INTO food_order.course_availability(course_id, flag, course_availability_date, week_number, is_active,
    is_deleted, created_by, session_id) VALUES(d_course_id, 'false', p_start_date+2, d_week_year_number, 'true', 
    'false', d_created_by, p_session_id);
INSERT INTO food_order.course_availability(course_id, flag, course_availability_date, week_number, is_active,
    is_deleted, created_by, session_id) VALUES(d_course_id, 'false', p_start_date+3, d_week_year_number, 'true', 
    'false', d_created_by, p_session_id);
INSERT INTO food_order.course_availability(course_id, flag, course_availability_date, week_number, is_active,
    is_deleted, created_by, session_id) VALUES(d_course_id, 'false', p_start_date+4, d_week_year_number, 'true', 
    'false', d_created_by, p_session_id);
INSERT INTO food_order.course_availability(course_id, flag, course_availability_date, week_number, is_active,
    is_deleted, created_by, session_id) VALUES(d_course_id, 'false', p_start_date+5, d_week_year_number, 'true', 
    'false', d_created_by, p_session_id);
INSERT INTO food_order.course_availability(course_id, flag, course_availability_date, week_number, is_active,
    is_deleted, created_by, session_id) VALUES(d_course_id, 'false', p_start_date+6, d_week_year_number, 
    'true', 'false', d_created_by, p_session_id);

ELSE RETURN -1;
END IF;

RETURN result;  
END;
$$
LANGUAGE plpgsql;

