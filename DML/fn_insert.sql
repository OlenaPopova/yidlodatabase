/********* INSERT FUNCTIONS **********/

/* FUNCTION ADMIN ADD COURSE TYPE */
CREATE OR REPLACE FUNCTION food_order.fn_admin_add_course_type(
  p_title varchar(40), 
  p_session_id bigint) 
RETURNS bigint 
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_created_by bigint;

BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_created_by;  

IF NOT EXISTS(SELECT  title 
      FROM   food_order.course_type
      WHERE  title = p_title) THEN
INSERT INTO food_order.course_type(title, is_deleted, created_by, session_id) 
VALUES (p_title, 'false', d_created_by, p_session_id);

ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN ADD COURSE */
CREATE OR REPLACE FUNCTION food_order.fn_admin_add_course(
  p_title varchar(80), 
  p_price decimal(5,2), 
  p_course_type_title varchar(40), 
  p_description varchar(200), 
  p_start_date date, 
  p_week_flags int, 
  p_session_id bigint) 
RETURNS bigint 
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_created_by bigint;
  d_course_type_id bigint;
  d_course_id bigint;
  d_week_number bigint;
  d_year_number bigint;
  d_week_year_number bigint;
  d_day_of_week_number integer;

  d_monday int;
  d_tuesday int;
  d_wednesday int;
  d_thursday int;
  d_friday int;
  d_saturday int;
  d_sunday int;

  flag_monday boolean; 
  flag_tuesday boolean; 
  flag_wednesday boolean; 
  flag_thursday boolean; 
  flag_friday boolean; 
  flag_saturday boolean; 
  flag_sunday boolean; 

BEGIN
SET search_path to 'food_order'; 

SELECT 1 << 1 INTO d_monday;
SELECT 1 << 2 INTO d_tuesday;
SELECT 1 << 3 INTO d_wednesday;
SELECT 1 << 4 INTO d_thursday;
SELECT 1 << 5 INTO d_friday;
SELECT 1 << 6 INTO d_saturday;
SELECT 1 << 7 INTO d_sunday;

IF ((p_week_flags & d_monday) = 0) THEN flag_monday = false; ELSE flag_monday = true; END IF;
IF ((p_week_flags & d_tuesday) = 0) THEN flag_tuesday = false; ELSE flag_tuesday = true; END IF;
IF ((p_week_flags & d_wednesday) = 0) THEN flag_wednesday = false; ELSE flag_wednesday = true; END IF;
IF ((p_week_flags & d_thursday) = 0) THEN flag_thursday = false; ELSE flag_thursday = true; END IF;
IF ((p_week_flags & d_friday) = 0) THEN flag_friday = false; ELSE flag_friday = true; END IF;
IF ((p_week_flags & d_saturday) = 0) THEN flag_saturday = false; ELSE flag_saturday = true; END IF;
IF ((p_week_flags & d_sunday) = 0) THEN flag_sunday = false; ELSE flag_sunday = true; END IF;

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_created_by;  

SELECT id FROM food_order.course_type
WHERE title = p_course_type_title INTO d_course_type_id; 

SELECT extract(dow FROM p_start_date::date) INTO d_day_of_week_number;

IF (d_day_of_week_number = 1) THEN

IF d_course_type_id IS NOT NULL THEN

INSERT INTO food_order.course(title, price, course_type_id, description, session_id,created_by) 
VALUES (p_title, p_price, d_course_type_id, p_description,  p_session_id, d_created_by);

d_course_id = (SELECT currval('food_order.course_id_seq'));

d_week_number = (SELECT extract(week FROM p_start_date::date));
d_year_number = (SELECT extract(year FROM p_start_date::date));
d_week_year_number = (SELECT CAST(CAST(d_week_number AS text)||CAST(d_year_number AS text) AS numeric(24,0)));

INSERT INTO food_order.course_availability(course_id, flag, course_availability_date, session_id,created_by, is_active, is_deleted, 
  week_number, week_flags) 
VALUES (d_course_id, flag_monday, p_start_date, p_session_id, d_created_by, 'true', 'false', d_week_year_number, p_week_flags);
INSERT INTO food_order.course_availability(course_id, flag, course_availability_date, session_id,created_by, is_active, is_deleted, 
  week_number, week_flags) 
VALUES (d_course_id, flag_tuesday, p_start_date+1, p_session_id, d_created_by, 'true', 'false', d_week_year_number, p_week_flags);
INSERT INTO food_order.course_availability(course_id, flag, course_availability_date, session_id,created_by, is_active, is_deleted, 
  week_number, week_flags) 
VALUES (d_course_id, flag_wednesday, p_start_date+2, p_session_id, d_created_by, 'true', 'false', d_week_year_number, p_week_flags);
INSERT INTO food_order.course_availability(course_id, flag, course_availability_date, session_id,created_by, is_active, is_deleted, 
  week_number, week_flags) 
VALUES (d_course_id, flag_thursday, p_start_date+3, p_session_id, d_created_by, 'true', 'false', d_week_year_number, p_week_flags);
INSERT INTO food_order.course_availability(course_id, flag, course_availability_date, session_id,created_by, is_active, is_deleted, 
  week_number, week_flags) 
VALUES (d_course_id, flag_friday, p_start_date+4, p_session_id, d_created_by, 'true', 'false', d_week_year_number, p_week_flags);
INSERT INTO food_order.course_availability(course_id, flag, course_availability_date, session_id,created_by, is_active, is_deleted, 
  week_number, week_flags) 
VALUES (d_course_id, flag_saturday, p_start_date+5, p_session_id, d_created_by, 'true', 'false', d_week_year_number,p_week_flags);
INSERT INTO food_order.course_availability(course_id, flag, course_availability_date, session_id,created_by, is_active, is_deleted, 
  week_number, week_flags) 
VALUES (d_course_id, flag_sunday, p_start_date+6, p_session_id, d_created_by, 'true', 'false', d_week_year_number,p_week_flags);

ELSE RETURN -1;
END IF;
ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;


/* FUNCTION ADMIN ADD CUSTOMER GROUP */
CREATE OR REPLACE FUNCTION food_order.fn_admin_add_customer_group(
  p_title varchar(40), 
  p_session_id bigint) 
RETURNS bigint 
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_created_by bigint;

BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_created_by;

IF NOT EXISTS(SELECT  title 
      FROM   food_order.customer_group
      WHERE  title = p_title) THEN
INSERT INTO food_order.customer_group(title, is_deleted,  session_id, created_by) 
VALUES (p_title, 'false',  p_session_id, d_created_by);

ELSE
RETURN -1;
END IF; 
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN ADD CUSTOMER */
CREATE OR REPLACE FUNCTION food_order.fn_admin_add_customer(
  p_first_name varchar(40), 
  p_last_name varchar(40), 
  p_login varchar(40), 
  p_password varchar(100), 
  p_customer_group_title varchar(40), 
  p_session_id bigint) 
RETURNS bigint 
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_created_by bigint;
  d_customer_group_id bigint;

BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_created_by;  

SELECT id FROM food_order.customer_group
WHERE title = p_customer_group_title INTO d_customer_group_id; 

IF d_customer_group_id IS NOT NULL THEN

IF NOT EXISTS(SELECT  login
      FROM   food_order.customer
      WHERE  login = p_login) THEN
INSERT INTO food_order.customer(first_name,last_name,login,password,is_active,
   customer_group_id, is_deleted, session_id, created_by) VALUES (p_first_name,p_last_name,p_login,
   p_password,'true', d_customer_group_id, 'false',  p_session_id, d_created_by);

ELSE RETURN -1;
END IF;
ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN ADD SESSION */
CREATE OR REPLACE FUNCTION food_order.fn_admin_add_session(
  p_customer_id bigint) 
RETURNS bigint 
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_session_id bigint;

BEGIN
SET search_path to 'food_order'; 

IF EXISTS(SELECT  id 
      FROM   food_order.customer as c
      WHERE  p_customer_id = c.id) THEN
INSERT INTO food_order.session(customer_id) VALUES (p_customer_id);
d_session_id = (SELECT currval('session_id_seq'));
RETURN d_session_id;

ELSE RETURN -1;
END IF; 
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN ADD DELIVERY */
CREATE OR REPLACE FUNCTION food_order.fn_admin_add_delivery(
  p_price decimal(7,3), 
  p_session_id bigint) 
RETURNS decimal(7,3) 
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_created_by bigint;

BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_created_by; 

IF  NOT EXISTS(SELECT current_date
      FROM   food_order.delivery
      WHERE  current_date = delivery_date) THEN
INSERT INTO food_order.delivery(price, delivery_date, created_by, session_id, is_deleted) 
VALUES (p_price,current_date,d_created_by,p_session_id, 'false');

ELSE 
UPDATE food_order.delivery SET price = p_price,
modified_by = d_created_by
WHERE delivery_date = current_date;

END IF; 
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ALL ADD COURSES ORDER */
CREATE OR REPLACE FUNCTION food_order.fn_all_add_courses_order(
  p_customer_id bigint, 
  p_order_date date, 
  p_session_id bigint) 
RETURNS bigint 
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_created_by bigint;
  d_order_id bigint;
  d_delivery_id bigint;

  d_delivery decimal(7,2);
  d_coins decimal(7,2);
  d_delivery_difference decimal(7,2);
  d_orders_amount decimal(7,2);
  d_null_delivery decimal(7,2);
  d_total_balance decimal(7,2);

BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_created_by;  

SELECT id FROM food_order.delivery
WHERE delivery_date <= p_order_date 
ORDER BY delivery_date DESC
LIMIT 1
INTO d_delivery_id;

SELECT (CAST(COUNT(c_o.id) AS decimal(7,2))) FROM food_order.courses_order AS c_o 
    WHERE  c_o.order_date = p_order_date AND c_o.id IN (SELECT o_d.order_id
FROM order_details AS o_d
INNER JOIN courses_order AS c_o ON o_d.order_id = c_o.id) INTO d_orders_amount;

IF (d_orders_amount = 0) THEN d_orders_amount = 1; END IF;

SELECT (ceiling((((fn_calculate_delivery_price(p_order_date))
/ d_orders_amount)))::decimal(7,2)) INTO d_delivery;

SELECT ((((fn_calculate_delivery_price(p_order_date))
/ d_orders_amount))::decimal(7,2)) INTO d_coins;

SELECT d_delivery - d_coins INTO d_delivery_difference;

IF NOT EXISTS(SELECT  customer_id 
      FROM   food_order.courses_order
      WHERE  p_customer_id = customer_id AND order_date = p_order_date ) THEN
INSERT INTO food_order.courses_order(customer_id, order_date, created_by, session_id, delivery_id) 
VALUES (p_customer_id, p_order_date, d_created_by, p_session_id, d_delivery_id);
d_order_id = (SELECT currval('food_order.courses_order_id_seq'));
RETURN d_order_id;
END IF;  

IF EXISTS(SELECT  customer_id 
      FROM   food_order.courses_order
      WHERE  p_customer_id = customer_id AND order_date = p_order_date ) THEN
SELECT id 
FROM food_order.courses_order AS c_o
WHERE c_o.customer_id = p_customer_id AND c_o.order_date = p_order_date
INTO d_order_id;
RETURN d_order_id;

ELSE RETURN -1;
END IF;  

RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ALL ADD COURSE TO ORDER */
CREATE OR REPLACE FUNCTION food_order.fn_all_add_course_to_order(
  p_order_id bigint,
  p_course_id bigint,
  p_portions integer,
  p_session_id bigint)

  RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_created_by bigint;
  d_course_price decimal(5,2);

BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_created_by;  

SELECT price FROM food_order.course
WHERE id = p_course_id INTO d_course_price;  

IF  NOT EXISTS(SELECT course_id
      FROM   food_order.order_details
      WHERE  p_course_id = course_id AND p_order_id = order_id ) THEN
IF EXISTS(SELECT course_id 
FROM course_availability AS c_a 
WHERE course_id = p_course_id AND is_deleted = false) THEN
INSERT INTO order_details(portions,course_id,order_id,course_price, session_id, created_by) 
VALUES (p_portions,p_course_id,p_order_id,d_course_price, p_session_id, d_created_by);

ELSE RETURN -1;

END IF; 
ELSE RETURN -1;
END IF; 
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN ADD EMAIL */
CREATE OR REPLACE FUNCTION food_order.fn_user_add_email(
  p_customer_id bigint, 
  p_email varchar(40), 
  p_session_id bigint) 
RETURNS bigint 
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_created_by bigint;

BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_created_by; 

IF EXISTS (SELECT id FROM food_order.customer WHERE id = p_customer_id) THEN
IF  NOT EXISTS(SELECT email
      FROM   food_order.customer
      WHERE  id = p_customer_id) THEN
UPDATE food_order.customer SET email = p_email,
created_by = d_created_by
WHERE p_customer_id = id;

ELSE 
UPDATE food_order.customer SET email = p_email,
modified_by = d_created_by
WHERE p_customer_id = id;

END IF; 
END IF; 
RETURN result;
END;
$$
LANGUAGE plpgsql;

