/********** DELETE FUNCTIONS **********/

/* FUNCTION ADMIN DEACTIVATE CUSTOMER */
CREATE OR REPLACE FUNCTION food_order.fn_admin_deactivate_customer(
    p_id bigint,
    p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;

BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_modified_by;  
      
IF EXISTS(SELECT  id 
      FROM   food_order.customer
      WHERE  id = p_id AND is_active='true') THEN
UPDATE food_order.customer SET is_active =  'false',
modified_by =  d_modified_by WHERE id = p_id;

ELSE RETURN -1;
END IF;

DELETE FROM food_order.order_details 
WHERE order_details.order_id IN 
(SELECT c_o.id
FROM food_order.courses_order AS c_o
WHERE c_o.customer_id = p_id);

DELETE FROM food_order.courses_order
WHERE customer_id = p_id;

RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN DELETE CUSTOMER */
CREATE OR REPLACE FUNCTION food_order.fn_admin_delete_customer(
    p_id bigint,
    p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;

BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_modified_by;  
      
IF EXISTS(SELECT  id 
      FROM   food_order.customer
      WHERE  id = p_id AND is_deleted='false') THEN

UPDATE food_order.customer SET is_active =  'false',
is_deleted =  'true',
modified_by = d_modified_by
WHERE id = p_id;

DELETE FROM food_order.order_details 
WHERE order_details.order_id IN 
(SELECT c_o.id
FROM food_order.courses_order AS c_o
WHERE c_o.customer_id = p_id);

DELETE FROM food_order.courses_order
WHERE customer_id = p_id;

ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN DEACTIVATE COURSE */
CREATE OR REPLACE FUNCTION food_order.fn_admin_deactivate_course(
    p_week_id bigint,
    p_course_id bigint,
    p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;
    
BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_modified_by;  
  
IF EXISTS(SELECT  week_number
      FROM   food_order.course_availability
      WHERE  p_week_id = week_number AND course_id = p_course_id AND is_active='true') THEN
UPDATE food_order.course_availability SET is_active =  'false',
modified_by = d_modified_by 
WHERE week_number = p_week_id AND course_id = p_course_id;

DELETE FROM food_order.order_details 
WHERE order_details.id IN (SELECT o_d.id
FROM order_details AS o_d
INNER JOIN courses_order AS c_o ON o_d.order_id = c_o.id
WHERE o_d.course_id = p_course_id);

ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN DELETE COURSE */
CREATE OR REPLACE FUNCTION food_order.fn_admin_delete_course(
    p_week_id bigint,
    p_course_id bigint,
    p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;
    
BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_modified_by;  
      
IF EXISTS(SELECT  week_number
      FROM   food_order.course_availability
      WHERE  p_week_id = week_number AND course_id = p_course_id AND is_deleted='false') THEN
UPDATE food_order.course_availability SET is_active =  'false',
is_deleted =  'true',
modified_by = d_modified_by 
WHERE week_number = p_week_id AND course_id = p_course_id;

DELETE FROM food_order.order_details 
WHERE order_details.id IN (SELECT o_d.id
FROM order_details AS o_d
INNER JOIN courses_order AS c_o ON o_d.order_id = c_o.id
WHERE o_d.course_id = p_course_id);

ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN ACTIVATE CUSTOMER */
CREATE OR REPLACE FUNCTION food_order.fn_admin_activate_customer(
    p_id bigint,
    p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;

BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_modified_by;  
      
IF EXISTS(SELECT  id 
      FROM   food_order.customer
      WHERE  id = p_id AND is_active='false') THEN
UPDATE food_order.customer SET is_active =  'true',
modified_by = d_modified_by WHERE id = p_id;

ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN ACTIVATE COURSE */
CREATE OR REPLACE FUNCTION food_order.fn_admin_activate_course(
    p_week_id bigint,
    p_course_id bigint,
    p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;
    
BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_modified_by;  
      
IF EXISTS(SELECT  week_number
      FROM   food_order.course_availability
      WHERE  p_week_id = week_number AND course_id = p_course_id AND is_active='false') THEN
UPDATE food_order.course_availability SET is_active =  'true',
modified_by = d_modified_by
WHERE week_number = p_week_id AND course_id = p_course_id;

ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN DELETE COURSE TYPE */
CREATE OR REPLACE FUNCTION food_order.fn_admin_delete_course_type(
    p_id bigint,
    p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;

BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_modified_by;   

IF  EXISTS(SELECT  id 
      FROM   food_order.course_type
      WHERE  p_id = id AND is_deleted='false') THEN
IF NOT EXISTS (SELECT c.id
FROM food_order.course AS c
WHERE c.course_type_id = p_id) THEN

UPDATE food_order.course_type SET is_deleted =  'true',
modified_by = d_modified_by WHERE id = p_id;

ELSE RETURN -1;
END IF;
ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN DELETE CUSTOMER GROUP */
CREATE OR REPLACE FUNCTION food_order.fn_admin_delete_customer_group(
    p_id bigint,
    p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;

BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_modified_by;  

IF  EXISTS(SELECT  c_g.id 
      FROM   food_order.customer_group AS c_g
      WHERE  p_id = c_g.id AND c_g.is_deleted='false') THEN
IF NOT EXISTS (SELECT cus.id
FROM food_order.customer AS cus
WHERE cus.customer_group_id = p_id) THEN
UPDATE food_order.customer_group SET is_deleted =  'true',
modified_by = d_modified_by WHERE id = p_id;

ELSE RETURN -1;
END IF;
ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ALL DELETE COURSES ORDER */
CREATE OR REPLACE FUNCTION food_order.fn_all_delete_courses_order(
  p_id bigint,
  p_session_id bigint)
RETURNS bigint
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;
  d_customer_id bigint;
  d_order_date date;

BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM session
WHERE id = p_session_id INTO d_modified_by;  
SELECT customer_id FROM courses_order
WHERE id = p_id INTO d_customer_id;
SELECT order_date FROM courses_order
WHERE id = p_id INTO d_order_date;

IF  EXISTS(SELECT  id 
      FROM   food_order.courses_order
      WHERE  p_id = id) THEN
UPDATE food_order.courses_order SET modified_by = d_modified_by WHERE id = p_id;
DELETE FROM food_order.order_details WHERE p_id = order_id;
DELETE FROM food_order.courses_order WHERE p_id = id;

ELSE RETURN -1;
END IF;
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN DELETE EMAIL */
CREATE OR REPLACE FUNCTION food_order.fn_user_delete_email(
  p_customer_id bigint, 
  p_session_id bigint) 
RETURNS bigint 
AS $$

DECLARE
  result bigint DEFAULT 0;
  d_modified_by bigint;

BEGIN
SET search_path to 'food_order'; 

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_modified_by; 

IF EXISTS(SELECT email
      FROM   food_order.customer
      WHERE  id = p_customer_id) THEN
UPDATE food_order.customer SET email=NULL
WHERE id = p_customer_id;

ELSE 
RETURN -1;

END IF; 
RETURN result;
END;
$$
LANGUAGE plpgsql;
