/********** AUDIT INSERT TRIGGERS **********/

/* TRIGGER INSERT ADD_COURSE TYPE */
CREATE OR REPLACE FUNCTION audit.fn_add_course_type()
RETURNS trigger
AS $BODY$

BEGIN    
SET search_path to 'audit';
INSERT INTO audit.course_type_audit(title, is_deleted, session_id, type, audit_date, created_by)
VALUES (NEW.title, 'false', NEW.session_id, 'Insert', CURRENT_TIMESTAMP, NEW.created_by);
RETURN NEW;
END;

$BODY$  
LANGUAGE plpgsql;

CREATE TRIGGER tr_add_course_type before INSERT ON "food_order".course_type
for each row execute procedure "audit".fn_add_course_type();

/* TRIGGER INSERT ADD COURSE */
CREATE OR REPLACE FUNCTION audit.fn_add_course()
RETURNS trigger
AS $BODY$

BEGIN    
SET search_path to 'audit'; 
INSERT INTO audit.course_audit(title, price,  course_type_id, description,  session_id, type, audit_date, created_by) 
VALUES (NEW.title, NEW.price,  NEW.course_type_id, NEW.description, NEW.session_id,  'Insert', CURRENT_TIMESTAMP, NEW.created_by);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_add_course before INSERT ON "food_order".course
for each row execute procedure "audit".fn_add_course();

/* TRIGGER INSERT ADD COURSE AVAILABILITY */
CREATE OR REPLACE FUNCTION audit.fn_add_course_availability()
RETURNS trigger
AS $BODY$

BEGIN 
SET search_path to 'audit';    
INSERT INTO audit.course_availability_audit(course_id, flag, course_availability_date, session_id,
  type, audit_date, created_by, is_active, is_deleted) 
VALUES (NEW.course_id, NEW.flag, NEW.course_availability_date, 
  NEW.session_id, 'Insert', CURRENT_TIMESTAMP, NEW.created_by, 'true', 'false');
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER  tr_add_course_availability before INSERT ON "food_order".course_availability
for each row execute procedure "audit".fn_add_course_availability();

/* TRIGGER INSERT ADD CUSTOMER GROUP */
CREATE OR REPLACE FUNCTION audit.fn_add_customer_group()
RETURNS trigger
AS $BODY$

BEGIN 
SET search_path to 'audit';   
INSERT INTO audit.customer_group_audit(title, is_deleted, session_id, type, audit_date, created_by) 
VALUES (NEW.title, 'false', NEW.session_id,'Insert', CURRENT_TIMESTAMP, NEW.created_by);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_add_customer_group before INSERT ON "food_order".customer_group
for each row execute procedure "audit".fn_add_customer_group();

/* TRIGGER INSERT ADD CUSTOMER */
CREATE OR REPLACE FUNCTION audit.fn_add_customer()
RETURNS trigger
AS $BODY$

BEGIN    
SET search_path to 'audit'; 
INSERT INTO audit.customer_audit(first_name, last_name, login, password, is_active, customer_group_id, 
  is_deleted, session_id,type, audit_date, created_by, email) 
VALUES (NEW.first_name, NEW.last_name, NEW.login, NEW.password, 'true', NEW.customer_group_id, 'false',  
	NEW.session_id,  'Insert', CURRENT_TIMESTAMP, NEW.created_by, NEW.email);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_add_customer before INSERT ON "food_order".customer
for each row execute procedure "audit".fn_add_customer();

/* TRIGGER INSERT ADD COURSES ORDER */
CREATE OR REPLACE FUNCTION audit.fn_add_courses_order()
RETURNS trigger
AS $BODY$

BEGIN    
SET search_path to 'audit'; 
INSERT INTO audit.courses_order_audit(customer_id, order_date, session_id, type, audit_date, created_by, delivery_id) 
VALUES (NEW.customer_id, NEW.order_date, NEW.session_id,  'Insert', CURRENT_TIMESTAMP, NEW.created_by, NEW.delivery_id);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_add_courses_order before INSERT ON "food_order".courses_order
for each row execute procedure "audit".fn_add_courses_order();

/* TRIGGER INSERT ADD ORDER DETAILS */
CREATE OR REPLACE FUNCTION audit.fn_add_order_details()
RETURNS trigger
AS $BODY$

DECLARE
d_course_price decimal(5,2);

BEGIN
SET search_path to 'audit'; 

SELECT price FROM food_order.course
WHERE id = NEW.course_id INTO d_course_price;

INSERT INTO audit.order_details_audit(portions, course_id, order_id, course_price, session_id, type, audit_date, created_by) 
VALUES (NEW.portions, NEW.course_id, NEW.order_id, d_course_price, 
	NEW.session_id, 'Insert', CURRENT_TIMESTAMP, NEW.created_by);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_add_order_details before INSERT ON "food_order".order_details
for each row execute procedure "audit".fn_add_order_details();

/* TRIGGER INSERT ADD DELIVERY */
CREATE OR REPLACE FUNCTION audit.fn_add_delivery()
RETURNS trigger
AS $BODY$

BEGIN
SET search_path to 'audit'; 
INSERT INTO audit.delivery_audit(price, delivery_date, created_by,  is_deleted, session_id, type, audit_date) 
VALUES (NEW.price, NEW.delivery_date, NEW.created_by, 'false',
  NEW.session_id, 'Insert', CURRENT_TIMESTAMP);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_add_delivery before INSERT ON "food_order".delivery
for each row execute procedure "audit".fn_add_delivery();
