/********** SELECT VIEWS AND FUNCTIONS **********/

/* VIEW ADMIN SELECT CUSTOMER (BY id) */
CREATE VIEW food_order.vw_admin_show_customer AS
SELECT c.id, c.first_name, c.last_name, c.login, c.password, c.is_active, c_g.title
FROM food_order.customer c
INNER JOIN food_order.customer_group c_g ON c_g.id = c.customer_group_id
WHERE c.is_deleted = 'false'
ORDER BY c.customer_group_id, c.first_name, c.last_name;
END;

/* FUNCTION ADMIN AUTHENTICATION CUSTOMER */
CREATE OR REPLACE FUNCTION food_order.fn_all_authentication(p_login varchar(40))
RETURNS TABLE (
    id bigint,
    first_name varchar(40),
    last_name varchar(40),
    login varchar(40),
    password varchar(100),
    is_active boolean,
    title varchar(40))

AS $$

BEGIN
SET search_path TO 'food_order';
IF EXISTS(SELECT c.login
FROM food_order.customer c
WHERE c.login = p_login) THEN
RETURN QUERY
SELECT c.id, c.first_name, c.last_name, c.login, c.password, c.is_active, c_g.title
FROM food_order.customer c
INNER JOIN food_order.customer_group c_g ON c_g.id = c.customer_group_id
WHERE c.login = p_login;
END IF;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN SHOW COURSES AVAILABILITY */
CREATE OR REPLACE FUNCTION food_order.fn_admin_show_courses_availability (p_week_number bigint) 
RETURNS TABLE (
    course_type_id bigint,
    week_number bigint,
    course_id bigint,
    title varchar(80),
    price decimal(5,2),
    week_flags int,
    is_active boolean)
AS $$

DECLARE
  week_flags int DEFAULT 0;

BEGIN
SET search_path TO 'food_order';

RETURN QUERY
SELECT DISTINCT c.course_type_id, c_a.week_number, c.id course_id, c.title, c.price, c_a.week_flags, c_a.is_active
FROM food_order.course c
INNER JOIN food_order.course_availability AS c_a ON c_a.course_id = c.id
WHERE is_deleted = 'false' AND c_a.week_number = p_week_number 
ORDER BY c.course_type_id, c.title;
END;

$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN SHOW COURSE */
CREATE OR REPLACE FUNCTION food_order.fn_admin_show_course (p_course_id bigint) 
RETURNS TABLE (
    course_id bigint,
    title varchar(80),
    price decimal(5,2),
    course_type varchar(40),
    description varchar(200),
    week_flags int)
AS $$

DECLARE

week_flags int DEFAULT 0;

BEGIN
SET search_path TO 'food_order';

RETURN QUERY
SELECT DISTINCT c.id course_id, c.title c_title, c.price, c_t.title c_t_title, c.description,
c_a.week_flags
FROM food_order.course c
INNER JOIN food_order.course_type c_t ON c_t.id = c.course_type_id
INNER JOIN food_order.course_availability c_a ON c_a.course_id = c.id
WHERE c_a.is_deleted = 'false' AND c.id = p_course_id;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN SHOW CUSTOMERS PER DAY (BY date) */
CREATE OR REPLACE FUNCTION food_order.fn_admin_show_customers_per_day(p_order_date date) 
RETURNS TABLE (
    customer_id bigint,
    first_name varchar(40),
    last_name varchar(40),
    login varchar(40))
AS $$

BEGIN
SET search_path TO 'food_order';

RETURN QUERY
SELECT  cus.id customer_id, cus.first_name, cus.last_name, cus.login
FROM food_order.customer AS cus
INNER JOIN food_order.courses_order AS c_o ON c_o.customer_id = cus.id
INNER JOIN food_order.order_details AS o_d ON c_o.id = o_d.order_id
INNER JOIN food_order.course_availability AS c_a ON c_a.course_id = o_d.course_id
WHERE c_o.order_date = p_order_date AND c_o.id = o_d.order_id
AND cus.is_active = 'true' AND cus.is_deleted = 'false' AND c_a.flag='true' 
AND c_a.course_availability_date=p_order_date
AND c_a.is_active='true' AND c_a.is_deleted='false'
ORDER BY cus.first_name, cus.last_name;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN SHOW ORDERS PER DAY (BY date, customer_id) */
CREATE OR REPLACE FUNCTION food_order.fn_admin_show_orders_per_day(p_order_date date, p_customer_id bigint) 
RETURNS TABLE (
    course_title varchar(80),
    course_price decimal(5,2),
    portions int,
    cost decimal(7,2),
    delivery_price decimal(7,2),
    id bigint)
AS $$

DECLARE
d_order_id bigint;
d_delivery decimal(7,2);
d_coins decimal(7,2);
d_delivery_difference decimal(7,2);
d_total_balance decimal(7,2);
d_orders_amount bigint;
d_first_order boolean;

BEGIN
SET search_path TO 'food_order';

SELECT c_o.id
FROM courses_order AS c_o
WHERE p_order_date = c_o.order_date AND p_customer_id = c_o.customer_id
INTO d_order_id;

SELECT (CAST(COUNT(c_o.id) AS decimal(7,2))) FROM food_order.courses_order AS c_o 
    WHERE  c_o.order_date = p_order_date AND c_o.id IN (SELECT o_d.order_id
FROM order_details AS o_d
INNER JOIN courses_order AS c_o ON o_d.order_id = c_o.id) INTO d_orders_amount;

IF (d_orders_amount = 0) THEN d_orders_amount = 1; END IF;

SELECT (ceiling((((fn_calculate_delivery_price(p_order_date))
/ d_orders_amount)))::decimal(7,2)) INTO d_delivery;

SELECT ((((fn_calculate_delivery_price(p_order_date))
/ d_orders_amount))::decimal(7,2)) INTO d_coins;

SELECT d_delivery - d_coins INTO d_delivery_difference;

IF EXISTS(SELECT d_order_id
FROM order_details AS o_d
WHERE o_d.order_id = d_order_id) THEN

RETURN QUERY
SELECT  cor.title, o_d.course_price, o_d.portions, 
SUM(cor.price*o_d.portions) AS cost, 

ceiling((((fn_calculate_delivery_price(p_order_date))
/ (SELECT (CAST(COUNT(c_o.id) AS decimal(7,2))) FROM food_order.courses_order AS c_o 
    WHERE  c_o.order_date = p_order_date AND c_o.id IN (SELECT o_d.order_id
FROM order_details AS o_d
INNER JOIN courses_order AS c_o ON o_d.order_id = c_o.id)))::decimal(7,2))) AS delivery_price,
o_d.id AS id
FROM food_order.courses_order AS c_o 
INNER JOIN food_order.order_details AS o_d ON o_d.order_id=c_o.id
INNER JOIN food_order.course AS cor ON o_d.course_id = cor.id 
INNER JOIN food_order.course_availability AS c_a ON c_a.course_id = o_d.course_id
INNER JOIN food_order.customer AS cus ON c_o.customer_id = cus.id
WHERE c_o.order_date = p_order_date AND c_o.customer_id = p_customer_id AND c_a.flag='true' 
AND c_a.course_availability_date = p_order_date AND c_a.is_active='true' AND c_a.is_deleted='false'
AND cus.is_active='true' AND cus.is_deleted='false' AND c_o.id=o_d.order_id
GROUP BY 1,2,3,5,6;
END IF;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ALL SHOW ORDER (BY date, customer_id) */
CREATE OR REPLACE FUNCTION food_order.fn_all_show_order(
    p_order_date date, 
    p_customer_id bigint) 
RETURNS TABLE (
    order_id  bigint, 
    order_date date,
    customer_id bigint,
    course_id bigint,
    course_title varchar(80),
    course_price decimal(5,2),
    portions int,
    cost decimal(7,2),
    delivery_price decimal(7,2))
AS $$

DECLARE 
d_order_id bigint;
d_delivery decimal(7,2);
d_coins decimal(7,2);
d_delivery_difference decimal(7,2);
d_orders_amount bigint;
d_total_balance decimal(7,2);
d_first_order boolean;

BEGIN
SET search_path TO 'food_order';

SELECT id 
FROM courses_order AS c_o
WHERE p_order_date = c_o.order_date AND p_customer_id = c_o.customer_id
INTO d_order_id;

SELECT (CAST(COUNT(c_o.id) AS decimal(7,2))) FROM food_order.courses_order AS c_o 
    WHERE  c_o.order_date = p_order_date AND c_o.id IN (SELECT o_d.order_id
FROM order_details AS o_d
INNER JOIN courses_order AS c_o ON o_d.order_id = c_o.id) INTO d_orders_amount;

IF (d_orders_amount = 0) THEN d_orders_amount = 1; END IF;

SELECT (ceiling((((fn_calculate_delivery_price(p_order_date))
/ d_orders_amount)))::decimal(7,2)) INTO d_delivery;

SELECT ((((fn_calculate_delivery_price(p_order_date))
/ d_orders_amount))::decimal(7,2)) INTO d_coins;

SELECT d_delivery - d_coins INTO d_delivery_difference;

IF EXISTS(SELECT d_order_id  
FROM order_details AS o_d
WHERE o_d.order_id = d_order_id) THEN

RETURN QUERY
SELECT DISTINCT c_o.id order_id, c_o.order_date, cus.id customer_id, cor.id course_id, cor.title, o_d.course_price,
o_d.portions, 
(((cor.price*o_d.portions)))::decimal(7,2) AS cost, 
ceiling((((fn_calculate_delivery_price(p_order_date))
/ (SELECT (CAST(COUNT(c_o.id) AS decimal(7,2))) FROM food_order.courses_order AS c_o 
   WHERE  c_o.order_date = p_order_date AND c_o.id IN (SELECT o_d.order_id
FROM order_details AS o_d
INNER JOIN courses_order AS c_o ON o_d.order_id = c_o.id)))::decimal(7,2))) AS delivery_price
FROM food_order.courses_order AS c_o 
INNER JOIN food_order.order_details AS o_d ON o_d.order_id=c_o.id
INNER JOIN food_order.course AS cor ON o_d.course_id = cor.id 
INNER JOIN food_order.course_availability AS c_a ON c_a.course_id = o_d.course_id
INNER JOIN food_order.customer AS cus ON c_o.customer_id = cus.id
WHERE
CASE WHEN p_order_date >= current_date THEN c_o.order_date = p_order_date AND cus.id = p_customer_id
AND c_a.flag='true' 
AND c_a.course_availability_date = p_order_date AND c_a.is_active='true' AND c_a.is_deleted='false'
AND cus.is_active='true' AND cus.is_deleted='false' AND c_o.id=o_d.order_id
ELSE
c_o.order_date = p_order_date AND cus.id = p_customer_id 
END
GROUP BY 1,2,3,4,5,6,7;
END IF;
END;
$$
LANGUAGE plpgsql;

/* VIEW ALL SHOW MENU (BY current date) */
CREATE OR REPLACE VIEW food_order.vw_all_show_menu_per_day AS
SELECT c.id, c.title course_title, c.price, c.description, c_t.title course_type_title, 
c_a.course_availability_date course_availability_date
FROM food_order.course AS c 
INNER JOIN food_order.course_type AS c_t ON c_t.id = c.course_type_id
INNER JOIN food_order.course_availability AS c_a ON c_a.course_id = c.id
WHERE c_a.is_deleted = 'false' AND c_a.is_active = 'true' AND c_a.flag = 'true';
END;

/* VIEW ALL SHOW COURSE TYPE */
CREATE OR REPLACE VIEW food_order.vw_all_show_course_type AS
SELECT c_t.id, c_t.title
FROM food_order.course_type AS c_t
WHERE c_t.is_deleted = 'false';
END;

/* FUNCTION SHOW CHEF REPORT (BY date) */
CREATE OR REPLACE FUNCTION food_order.fn_admin_show_chef_report(p_order_date date) 
RETURNS TABLE (
    id bigint,
    title varchar(80),
    description varchar(200),
    price decimal(5,2),
    count bigint,
    delivery decimal(7,2))
AS $$

DECLARE
d_previous_delivery decimal(7,2);

BEGIN 
SET search_path TO 'food_order';

SELECT d.price 
FROM food_order.delivery AS d 
WHERE d.delivery_date <= p_order_date AND d.price != 0 
ORDER BY d.delivery_date DESC
LIMIT 1
INTO d_previous_delivery;

RETURN QUERY
SELECT  cor.id, cor.title, cor.description, cor.price, SUM(o_d.portions) AS count, 
(CASE WHEN (ceiling(fn_calculate_delivery_price(p_order_date)) = 0) THEN d_previous_delivery
ELSE ceiling(fn_calculate_delivery_price(p_order_date)) END) AS delivery
FROM food_order.courses_order AS c_o
INNER JOIN food_order.order_details AS o_d ON c_o.id = o_d.order_id
INNER JOIN food_order.course AS cor ON o_d.course_id = cor.id 
INNER JOIN food_order.course_availability AS c_a ON c_a.course_id = o_d.course_id 
WHERE c_o.order_date = p_order_date AND c_a.flag='true' AND c_a.course_availability_date = p_order_date
GROUP BY 1,2,3, 4, c_o.order_date;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION SHOW THE LATEST DELIVERY */
CREATE OR REPLACE FUNCTION food_order.fn_admin_show_delivery() 
RETURNS TABLE (
    delivery decimal(5,2))
AS $$

BEGIN 
SET search_path TO 'food_order';

RETURN QUERY
SELECT  ceiling(d.price)
FROM food_order.delivery AS d
WHERE delivery_date <= current_date
ORDER BY delivery_date DESC
LIMIT 1;
END;
$$
LANGUAGE plpgsql;

/*FUNCTION SHOW ALL COURSES */
CREATE OR REPLACE FUNCTION food_order.fn_admin_show_all_courses()
RETURNS TABLE (
id bigint, 
title varchar(80))
AS $$

BEGIN 
SET search_path TO 'food_order';

RETURN QUERY
SELECT max(cor.id) AS id, cor.title
FROM course AS cor
INNER JOIN course_availability AS c_a ON c_a.course_id = cor.id 
/*WHERE c_a.is_deleted='false' AND c_a.course_availability_date >= (current_date-30)*/
WHERE c_a.is_deleted='false' AND c_a.course_availability_date >= 
date_trunc('day', NOW() - interval '1 month')
GROUP BY cor.title
ORDER BY cor.title;

END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN SHOW EMAIL BY CUSTOMER_ID */
CREATE OR REPLACE FUNCTION food_order.fn_user_show_email(p_customer_id bigint) 
RETURNS TABLE(
id bigint,
email varchar(40)
)
AS $$

DECLARE
d_email varchar(40);

BEGIN
SET search_path to 'food_order'; 

IF EXISTS (SELECT c.id FROM customer AS c
    WHERE c.id = p_customer_id) THEN

IF EXISTS (SELECT c.email from food_order.customer as c WHERE c.id = p_customer_id) THEN
RETURN QUERY
SELECT  c.id, c.email
FROM food_order.customer AS c
WHERE c.id = p_customer_id AND c.email IS NOT NULL AND c.is_deleted='false' AND c.is_active='true';

END IF;
END IF;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN SHOW EMAILS */
CREATE OR REPLACE FUNCTION food_order.fn_admin_show_emails() 
RETURNS TABLE (
id bigint,
email varchar(40))
AS $$

DECLARE
d_email varchar(40);

BEGIN
SET search_path to 'food_order'; 

RETURN QUERY

SELECT  c.id, c.email
FROM food_order.customer AS c
WHERE c.email IS NOT NULL AND c.is_deleted='false' AND c.is_active='true';

END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN SHOW CUSTOMERS FOR EXCEL */
CREATE OR REPLACE FUNCTION food_order.fn_admin_show_customers_for_excel(p_current_date date) 
RETURNS TABLE (
id bigint,
last_name varchar(40),
total_cost decimal(7,2))
AS $$

DECLARE
d_delivery decimal(7,2);
d_coins decimal(7,2);
d_delivery_difference decimal(7,2);
d_orders_amount bigint;
d_total_balance decimal(7,2);
is_sum_zero boolean;
d_first_order boolean;

BEGIN
SET search_path to 'food_order'; 

SELECT (CAST(COUNT(c_o.id) AS decimal(7,2))) FROM food_order.courses_order AS c_o 
    WHERE  c_o.order_date = p_current_date AND c_o.id IN (SELECT o_d.order_id
FROM order_details AS o_d
INNER JOIN courses_order AS c_o ON o_d.order_id = c_o.id) INTO d_orders_amount;

IF (d_orders_amount = 0) THEN d_orders_amount = 1; END IF;

SELECT (ceiling((((fn_calculate_delivery_price(p_current_date))
/ d_orders_amount)))::decimal(7,2)) INTO d_delivery;

SELECT ((((fn_calculate_delivery_price(p_current_date))
/ d_orders_amount))::decimal(7,2)) INTO d_coins;

SELECT d_delivery - d_coins INTO d_delivery_difference;

RETURN QUERY

SELECT  cus.id, cus.last_name,   (SUM(o_d.course_price * o_d.portions)) + 
ceiling((((fn_calculate_delivery_price(p_current_date))
/ (SELECT (CAST(COUNT(c_o.id) AS decimal(7,2))) FROM food_order.courses_order AS c_o 
   WHERE  c_o.order_date = p_current_date AND c_o.id IN (SELECT o_d.order_id
FROM order_details AS o_d
INNER JOIN courses_order AS c_o ON o_d.order_id = c_o.id)))::decimal(7,2)))::decimal(7,2) 
AS total_cost
FROM food_order.customer AS cus 
INNER JOIN food_order.courses_order AS c_o ON c_o.customer_id = cus.id
INNER JOIN food_order.order_details AS o_d ON o_d.order_id = c_o.id
WHERE c_o.order_date = p_current_date
GROUP BY cus.id, cus.last_name
ORDER BY cus.last_name;

END;
$$
LANGUAGE plpgsql;

/* FUNCTION ADMIN SHOW COURSES FOR EXCEL */
CREATE OR REPLACE FUNCTION food_order.fn_admin_show_courses_for_excel(
    p_current_date date, 
    p_course_type varchar(40)) 
RETURNS TABLE (
id bigint,
title varchar(80),
description varchar(200))
AS $$

DECLARE
d_course_type_id bigint;

BEGIN
SET search_path to 'food_order'; 

SELECT c_t.id
FROM course_type AS c_t
WHERE c_t.title = p_course_type INTO d_course_type_id;

RETURN QUERY

SELECT  c.id, c.title, c.description
FROM food_order.course AS c
INNER JOIN food_order.course_availability AS c_a ON c_a.course_id = c.id
WHERE c_a.flag = 'true' AND c_a.is_active='true' AND c_a.is_deleted='false' 
AND c_a.course_availability_date = p_current_date AND c.course_type_id = d_course_type_id
ORDER BY c.id;

END;
$$
LANGUAGE plpgsql;


/* FUNCTION ADMIN SHOW PORTIONS FOR EXCEL */
CREATE OR REPLACE FUNCTION food_order.fn_admin_show_portions_for_excel(
    p_current_date date, 
    p_customer_id bigint) 
RETURNS TABLE (
id bigint,
portions bigint)
AS $$

BEGIN
SET search_path to 'food_order'; 

RETURN QUERY
SELECT  nextval('portions_id'),
SUM(CASE WHEN c_o.customer_id = p_customer_id AND o_d.portions IS NOT NULL THEN o_d.portions ELSE 0 END) AS portions
FROM food_order.course AS cor
INNER JOIN food_order.course_availability AS c_a ON c_a.course_id = cor.id
LEFT JOIN food_order.courses_order AS c_o ON c_o.customer_id = p_customer_id
LEFT JOIN food_order.order_details AS o_d ON o_d.order_id = c_o.id AND o_d.course_id = cor.id

WHERE 
c_a.course_availability_date = p_current_date AND c_a.flag='true' 
AND (c_o.order_date = p_current_date OR c_o.order_date IS NULL) AND c_a.is_deleted='false' 
AND c_a.is_active='true'
GROUP BY cor.id
ORDER BY cor.course_type_id, cor.id;

END;
$$
LANGUAGE plpgsql;

---correct
SELECT  nextval('portions_id'),
SUM(CASE WHEN c_o.customer_id = p_customer_id THEN o_d.portions ELSE 0 END) AS portions
FROM food_order.course AS cor
INNER JOIN food_order.course_availability AS c_a ON c_a.course_id = cor.id
LEFT JOIN food_order.order_details AS o_d ON cor.id = o_d.course_id
LEFT JOIN food_order.courses_order AS c_o ON c_o.id = o_d.order_id
WHERE 
c_a.course_availability_date = p_current_date AND c_a.flag='true' 
AND (c_o.order_date = p_current_date OR c_o.order_date IS NULL) AND c_a.is_deleted='false' 
AND c_a.is_active='true'
GROUP BY cor.id
ORDER BY cor.course_type_id, cor.id;
--- old version
SELECT  nextval('portions_id'),
SUM(CASE WHEN c_o.customer_id = 2 THEN o_d.portions ELSE 0 END) AS portions
FROM food_order.course AS cor
INNER JOIN food_order.course_availability AS c_a ON c_a.course_id = cor.id
INNER JOIN food_order.order_details AS o_d ON c_a.course_id = o_d.course_id
INNER JOIN food_order.courses_order AS c_o ON c_o.customer_id = 2 AND c_o.order_date = '2017-09-12'
WHERE 
c_a.course_availability_date = '2017-09-12' AND c_a.flag='true' 
AND (c_o.order_date = '2017-09-12' OR c_o.order_date IS NULL) AND c_a.is_deleted='false' 
AND c_a.is_active='true'
GROUP BY cor.id
ORDER BY cor.course_type_id, cor.id;

/* FUNCTION USER LOGOUT */
CREATE OR REPLACE FUNCTION food_order.fn_user_logout(
    p_session_id bigint) 
RETURNS bigint AS $$

DECLARE
  result bigint DEFAULT 0;

BEGIN
SET search_path to 'food_order'; 
IF EXISTS(SELECT  id 
    FROM   session as s
    WHERE  p_session_id = s.id) THEN
UPDATE food_order.session SET logout_date = now() WHERE id = p_session_id;
RETURN 0;

ELSE RETURN -1;
END IF; 
RETURN result;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION CALCULATE DELIVERY PRICE */
CREATE OR REPLACE FUNCTION food_order.fn_calculate_delivery_price(
    p_order_date date) 
RETURNS decimal(7,2) AS $$

DECLARE
  d_delivery_price decimal(7,2) DEFAULT 0;
  d_total_balance decimal(7,2);
  d_first_order boolean;

BEGIN
SET search_path to 'food_order';

SELECT d.price FROM food_order.delivery AS d 
WHERE delivery_date <= p_order_date 
ORDER BY delivery_date DESC
LIMIT 1
INTO d_delivery_price;

RETURN d_delivery_price;
END;
$$
LANGUAGE plpgsql;

/* FUNCTION SET DELIVERY PRICE TO ZERO */
CREATE OR REPLACE FUNCTION food_order.fn_set_delivery_price_to_zero(p_session_id bigint) 
RETURNS bigint AS $$

DECLARE
  result bigint DEFAULT 0;
  d_created_by bigint;

BEGIN
SET search_path to 'food_order';

SELECT customer_id FROM food_order.session
WHERE id = p_session_id INTO d_created_by;

IF EXISTS(SELECT current_date FROM food_order.delivery WHERE delivery_date = current_date) THEN
UPDATE food_order.delivery SET price = 0 WHERE delivery_date = current_date;
ELSE
INSERT INTO food_order.delivery(price, delivery_date, created_by, session_id, is_deleted) 
VALUES (0,current_date,d_created_by,p_session_id, 'false');
END IF;

IF NOT EXISTS (SELECT current_date FROM food_order.delivery WHERE delivery_date = current_date+1) THEN
INSERT INTO food_order.delivery(price, delivery_date, created_by, session_id, is_deleted) 
VALUES (d_delivery_price,current_date+1,d_created_by,p_session_id, 'false');
END IF;

RETURN result;
END;
$$
LANGUAGE plpgsql;
