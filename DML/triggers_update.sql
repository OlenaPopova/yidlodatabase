/********* AUDIT UPDATE AND DELETE TRIGGERS **********/

/* TRIGGER UPDATE CUSTOMER GROUP */
CREATE OR REPLACE FUNCTION audit.fn_update_customer_group()
RETURNS trigger
AS $BODY$

BEGIN    
SET search_path to 'audit';
INSERT INTO customer_group_audit(title, is_deleted, modified_by, session_id, type, audit_date) 
VALUES (NEW.title, NEW.is_deleted, NEW.modified_by, NEW.session_id, 'Update', CURRENT_TIMESTAMP);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_update_customer_group before UPDATE ON "food_order".customer_group
for each row execute procedure "audit".fn_update_customer_group();

/* TRIGGER UPDATE CUSTOMER */
CREATE OR REPLACE FUNCTION audit.fn_update_customer()
RETURNS trigger
AS $BODY$

BEGIN    
SET search_path to 'audit';
INSERT INTO customer_audit(first_name, last_name, login, password, is_active, customer_group_id, 
  is_deleted, modified_by, session_id, type,audit_date) 
VALUES (NEW.first_name, NEW.last_name, NEW.login, NEW.password, NEW.is_active, NEW.customer_group_id, NEW.is_deleted, 
  NEW.modified_by, NEW.session_id, 'Update', CURRENT_TIMESTAMP);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_update_customer before UPDATE ON "food_order".customer
for each row execute procedure "audit".fn_update_customer();

/* TRIGGER UPDATE COURSE TYPE */
CREATE OR REPLACE FUNCTION audit.fn_update_course_type()
RETURNS trigger
AS $BODY$

BEGIN    
SET search_path to 'audit';
INSERT INTO course_type_audit(title, is_deleted, modified_by, session_id, type, audit_date) 
VALUES (NEW.title, NEW.is_deleted, NEW.modified_by, NEW.session_id, 'Update', CURRENT_TIMESTAMP);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_update_course_type before UPDATE ON "food_order".course_type
for each row execute procedure "audit".fn_update_course_type();

/* TRIGGER UPDATE COURSE */
CREATE OR REPLACE FUNCTION audit.fn_update_course()
RETURNS trigger
AS $BODY$

BEGIN   
SET search_path to 'audit'; 
INSERT INTO course_audit(title, price,  course_type_id, description,  
 session_id,type, audit_date, modified_by) 
VALUES (NEW.title, NEW.price, NEW.course_type_id, NEW.description, 
	NEW.session_id,  'Update', CURRENT_TIMESTAMP, NEW.modified_by);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_update_course before UPDATE ON "food_order".course
for each row execute procedure "audit".fn_update_course();

/* TRIGGER UPDATE COURSE AVAILABILITY */
CREATE OR REPLACE FUNCTION audit.fn_update_course_availability()
RETURNS trigger
AS $BODY$

BEGIN    
INSERT INTO audit.course_availability_audit(course_id, flag, course_availability_date, modified_by, 
  session_id, type,audit_date, is_active, is_deleted) 
VALUES (NEW.course_id, NEW.flag, NEW.course_availability_date, NEW.modified_by, 
	NEW.session_id, 'Update', CURRENT_TIMESTAMP, 'true', 'false');
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_update_course_availability before UPDATE ON "food_order".course_availability
for each row execute procedure "audit".fn_update_course_availability();

/* TRIGGER UPDATE COURSES ORDER */
CREATE OR REPLACE FUNCTION audit.fn_update_courses_order()
RETURNS trigger
AS $BODY$

BEGIN    
SET search_path to 'audit'; 
INSERT INTO courses_order_audit(customer_id, order_date, session_id, type,audit_date, created_by, delivery_id) 
VALUES (NEW.customer_id, NEW.order_date, NEW.session_id, 'Update', CURRENT_TIMESTAMP, NEW.created_by, 
  NEW.delivery_id);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_update_courses_order before INSERT ON "food_order".courses_order
for each row execute procedure "audit".fn_update_courses_order();

/* TRIGGER UPDATE ORDER DETAILS */
CREATE OR REPLACE FUNCTION audit.fn_update_order_details()
RETURNS trigger
AS $BODY$

DECLARE
  d_course_price decimal(5,2);

BEGIN    
SET search_path to 'audit';

SELECT price FROM food_order.course
WHERE id = NEW.course_id INTO d_course_price;

INSERT INTO order_details_audit(portions, course_id, course_price, order_id, modified_by, session_id, type, audit_date) 
VALUES (NEW.portions, NEW.course_id, d_course_price, NEW.order_id, NEW.modified_by, NEW.session_id, 'Update', CURRENT_TIMESTAMP);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_update_order_details before UPDATE ON "food_order".order_details
for each row execute procedure "audit".fn_update_order_details();

/* TRIGGER UPDATE DELIVERY */
CREATE OR REPLACE FUNCTION audit.fn_update_delivery()
RETURNS trigger
AS $BODY$

BEGIN
SET search_path to 'audit'; 
INSERT INTO audit.delivery_audit(price, delivery_date, created_by,  is_deleted, session_id, type,audit_date) 
VALUES (NEW.price, NEW.delivery_date, NEW.created_by, NEW.is_deleted,
  NEW.session_id, 'Update', CURRENT_TIMESTAMP);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_update_delivery before UPDATE ON "food_order".delivery
for each row execute procedure "audit".fn_update_delivery();

/* TRIGGER DELETE ORDER DETAILS */
CREATE OR REPLACE FUNCTION audit.fn_delete_order_details()
RETURNS trigger
AS $BODY$

BEGIN    
SET search_path to 'audit';

INSERT INTO order_details_audit(portions, course_id, course_price, order_id, modified_by, session_id, type, audit_date) 
VALUES (OLD.portions, OLD.course_id, OLD.course_price, OLD.order_id, OLD.modified_by, OLD.session_id, 'Delete', CURRENT_TIMESTAMP);
RETURN OLD;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_delete_order_details before DELETE ON "food_order".order_details
for each row execute procedure "audit".fn_delete_order_details();

/* TRIGGER DELETE COURSES ORDER */
CREATE OR REPLACE FUNCTION audit.fn_delete_courses_order()
RETURNS trigger
AS $BODY$

BEGIN    
SET search_path to 'audit'; 
INSERT INTO audit.courses_order_audit(customer_id, order_date, session_id, type,audit_date, created_by, delivery_id) 
VALUES (OLD.customer_id, OLD.order_date, OLD.session_id, 'Delete', CURRENT_TIMESTAMP, OLD.created_by, 
  OLD.delivery_id);
RETURN OLD;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_delete_courses_order before DELETE ON "food_order".courses_order
for each row execute procedure "audit".fn_delete_courses_order();

/* TRIGGER UPDATE PASSWORD */
CREATE OR REPLACE FUNCTION audit.fn_update_password()
RETURNS trigger
AS $BODY$

BEGIN    
SET search_path to 'audit';
INSERT INTO customer_audit(first_name, last_name, login, password, is_active, customer_group_id, 
  is_deleted, modified_by, session_id, type,audit_date, email) 
VALUES (OLD.first_name, OLD.last_name, OLD.login, NEW.password, OLD.is_active, OLD.customer_group_id, OLD.is_deleted, 
  NEW.modified_by, NEW.session_id, 'Update', CURRENT_TIMESTAMP, OLD.email);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_update_password before UPDATE ON "food_order".customer
for each row execute procedure "audit".fn_update_password();

/* TRIGGER INSERT ADD EMAIL */
CREATE OR REPLACE FUNCTION audit.fn_update_email()
RETURNS trigger
AS $BODY$

BEGIN    
SET search_path to 'audit'; 
INSERT INTO customer_audit(first_name, last_name, login, password, is_active, customer_group_id, 
  is_deleted, session_id,type, audit_date, created_by, email) 
VALUES (NEW.first_name, NEW.last_name, OLD.login, OLD.password, 'true', OLD.customer_group_id, 'false',  
	NEW.session_id,  'Insert', CURRENT_TIMESTAMP, NEW.created_by, NEW.email);
RETURN NEW;
END;

$BODY$
LANGUAGE plpgsql;

CREATE TRIGGER tr_update_email before UPDATE ON "food_order".customer
for each row execute procedure "audit".fn_update_email();

/* TRIGGER FUNCTION UPDATE MODIFIED DATE */
CREATE FUNCTION food_order.update_modified_date()
    RETURNS trigger
    LANGUAGE 'plpgsql'
AS $BODY$

BEGIN
    NEW.modified_date = now();
    RETURN NEW; 
END;

$BODY$;